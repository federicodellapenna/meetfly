package sample;

import javafx.fxml.FXML;

//Metodo che gestisce il logout dal servizio

public class ControllerLogout extends Main {
    String email;
    public void setEmailAddr(String e) { email = e; }

    @FXML
    public void handleNo(){
        MainLogoutScene.getWindow().hide(); //in caso di non voler compiere il logout chiude tale finestra
                                            // con il conseguente ritorno alla finestra primaria di ricerca

    }

    public void handleYes(){
        startSafeLogout(email); //In caso di logout apertura altra finestra per salvataggio dei dati o meno
                                // a discrezione dell'utente


    }


    }