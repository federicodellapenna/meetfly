package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
Classe che si occupa delle informazione degli utenti da mostrare in tabella
 */

public class Users {
    private StringProperty email;
    private StringProperty airport;
    private StringProperty flight;
    private StringProperty city;
    private StringProperty cityCode;
    private StringProperty street;
    private StringProperty number;

    //Default constructor

    public Users(){
        this(null);
    }
    public Users(String username,String airport, String flight, String city, String cityCode, String street, String number)  {
        this.email= new SimpleStringProperty(username);
        this.airport=new SimpleStringProperty(airport);
        this.flight=new SimpleStringProperty(flight);
        this.city=new SimpleStringProperty(city);
        this.cityCode=new SimpleStringProperty(cityCode);
        this.street=new SimpleStringProperty(street);
        this.number=new SimpleStringProperty(number);
    }

    public Users(String username)  {
        this.email= new SimpleStringProperty(username);
        this.airport=new SimpleStringProperty("Not specified");
        this.flight=new SimpleStringProperty("Not specified");
        this.city=new SimpleStringProperty("Not specified");
        this.cityCode=new SimpleStringProperty("Not specified");
        this.street=new SimpleStringProperty("Not specified");
        this.number=new SimpleStringProperty("Not specified");
    }
    public String getEmail(){return email.get();}
    public void setEmail(String email){this.email.set(email);}
    public StringProperty emailProperty(){return email;}

    public String getAirport(){return airport.get();}
    public void setAirport(String airport){this.airport.set(airport);}
    public StringProperty airportProperty(){return airport;}

    public String getFlight(){return flight.get();}
    public void setFlight(String flight){this.flight.set(flight);}
    public StringProperty flightlProperty(){return flight;}

    public String getCity(){return city.get();}
    public void setCity(String city){this.city.set(city);}
    public StringProperty cityProperty(){return city;}

    public String getCityCode(){return cityCode.get();}
    public void setCityCode(String cityCode){this.cityCode.set(cityCode);}
    public StringProperty cityCodeProperty(){return cityCode;}

    public String getStreet(){return street.get();}
    public void setStreet(String street){this.street.set(street);}
    public StringProperty streetProperty(){return street;}

    public String getNumber(){return number.get();}
    public void setNumber(String number){this.number.set(number);}
    public StringProperty numberProperty(){return number;}
}
