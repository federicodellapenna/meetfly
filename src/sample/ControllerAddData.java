package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.awt.*;
import java.sql.*;
import java.util.List;
import java.util.Vector;

/*
Questa classe gestisce la finestra della visualizzazione dei personal data.
Ci sono i controlli per le immissioni dei dati degli utenti in tabella nelle caselle
e il controllo dei bottoni back e forward.
 */

public class ControllerAddData extends Main {
    private static String url = "jdbc:mysql://localhost:3306/meetfly?useSSL=false&serverTimezone=UTC";
    private static String username = "root";
    private static String password = "root";
    private Vector<String> unames;
    private int index;
    @FXML private TextField nickBox;
    @FXML private TextField natBox;
    @FXML private TextField byBox;
    @FXML private TextField sexBox;
    @FXML private TextField phoneBox;
    @FXML private Button back;
    @FXML private Button forward;

    /*
    Metodo chiamato dal Main in fase di inizializzazione della finestra.
    Riceve in ingresso un vector contenente tutti i nickname che sono i risultati dell'ultima ricerca
    e il nickname selezionato in tabella.
    In seguito, a partire dal nickname scelto, vengono estratti gli altri dati dell'utente presenti nel database
    inserendo nelle caselle solo quelli personali(se inseriti dallo stesso).
     */
    @FXML
    public void setUnames(Vector<String> u, String d) throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
         unames=u;
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM meetfly.users WHERE username=?");
        pstmt.setString(1,d);
        index=unames.indexOf(d);
        nickBox.setText(unames.elementAt(index));
        ResultSet rs=pstmt.executeQuery();
        while (rs.next()){
            natBox.setText(rs.getString("nationality"));
            byBox.setText(rs.getString("birthyear"));
            sexBox.setText(rs.getString("sex"));
            phoneBox.setText(rs.getString("phone"));
        }
        if(unames.firstElement().equals(d)){  //Controllo di indice. Se l'elemento è il primo
            back.setDisable(true);            // viene disabilitato il back button
        }
        if(unames.lastElement().equals(d)){   //Stesso controllo ma con l'ultimo elemento e
            forward.setDisable(true);         //la disabilitazione del forward button
        }
        conn.close();

    }

    /*
    Gestisce l'avanzamento dell'indice di scorrimento, estrando i dati corrispondenti al nickname e
    controllando che non sia nè il primo nè l'ultimo. Provvede inoltre alla disabilitazione degli stessi
    in uno dei due casi sopracitati o in entrambi.
     */
    @FXML
    public void handleForward() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        index++;
        back.setDisable(false);
        forward.setDisable(false);
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM meetfly.users WHERE username=?");
        pstmt.setString(1,unames.elementAt(index));
        nickBox.setText(unames.elementAt(index));
        ResultSet rs=pstmt.executeQuery();
        while (rs.next()){
            natBox.setText(rs.getString("nationality"));
            byBox.setText(rs.getString("birthyear"));
            sexBox.setText(rs.getString("sex"));
            phoneBox.setText(rs.getString("phone"));
        }
        if(index==0){
            back.setDisable(true);
        }
        if(index==unames.size()-1){
            forward.setDisable(true);
        }
        conn.close();

    }

     /*
    Gestisce il decremento dell'indice di scorrimento, estrando i dati corrispondenti al nickname e
    controllando che non sia nè il primo nè l'ultimo. Provvede inoltre alla disabilitazione degli stessi
    in uno dei due casi sopracitati o in entrambi.
     */

    @FXML
    public void handleBack() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        index--;
        back.setDisable(false);
        forward.setDisable(false);
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM meetfly.users WHERE username=?");
        pstmt.setString(1,unames.elementAt(index));
        nickBox.setText(unames.elementAt(index));
        ResultSet rs=pstmt.executeQuery();
        while (rs.next()){
            natBox.setText(rs.getString("nationality"));
            byBox.setText(rs.getString("birthyear"));
            sexBox.setText(rs.getString("sex"));
            phoneBox.setText(rs.getString("phone"));
        }
        if(index==0){
            back.setDisable(true);
        }
        if(index==unames.size()-1){
            forward.setDisable(true);
        }
        conn.close();

    }
}
