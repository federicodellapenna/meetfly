package sample;


import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;


import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

/**
 * A utility class for sending e-mail with attachment.
 *
 * @author www.codejava.net
 *
 */
public class EmailUtility {


    /**
     * This methos is used to set  parameters for sending Email
     * @param host
     * @param port
     * @param userName : the email of the sender
     * @param password
     * @param email
     * @param subject : the subject of the email
     * @param message : message
     * @throws MessagingException
     * @throws MessagingException
     */

    public static void sendEmail(String host, String port,
                                 final String userName, final String password, String email,
                                 String subject, String message) throws AddressException,
            MessagingException {
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.user", userName);
        properties.put("mail.password", password);

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };
        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = { new InternetAddress(email) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        msg.setContent(message, "text/plain");

        // creates message part
        // MimeBodyPart messageBodyPart = new MimeBodyPart();
        // messageBodyPart.setContent(message, "text/plain");

        // creates multi-part
        // Multipart multipart = new MimeMultipart();
        // multipart.addBodyPart(messageBodyPart);

        /*
         * // adds attachments if (attachFile != null) { MimeBodyPart attachPart
         * = new MimeBodyPart();
         *
         * try { attachPart.attachFile(attachFile); } catch (IOException ex) {
         * ex.printStackTrace(); }
         *
         * multipart.addBodyPart(attachPart); }
         */
        // sets the multi-part as e-mail's content
        // msg.setContent(multipart);
        msg.setContent(message, "text/plain");

        // sends the e-mail
        Transport.send(msg);

    }

    public static void main(String args[]) throws MessagingException {
		/*
					<!-- SMTP configuration -->
			<param name="host">smtp.gmail.com</param>
			<param name="port">587</param>

			<!-- User: email -->
			<param name="userName">aailabunicas@gmail.com</param>
			<param name="password">#Aa1lab2019#</param>
			<!-- <param name="downloadHost">localhost</param> -->
			<param name="downloadHost">p101.unicas.it</param> */


        sendEmail("smtp.libero.it", "25", "meetfly2019@libero.it", "noreplymeetfly",
                "marialuisasimone@gmail.com", "Welcome to MeetFly!", "Now you are a member of MeetFly Community :)");

    }

}