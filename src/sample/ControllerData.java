package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import org.controlsfx.control.textfield.TextFields;

import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

/*
Classe che gestisce l'inserimento dei dati personali facoltativi (nazionalità, sesso, anno di nascita e telefono)
e il loro relativo salvataggio nel database.
 */

public class ControllerData  extends Main implements Initializable {

    private static String url="jdbc:mysql://localhost:3306/meetfly?useSSL=false&serverTimezone=UTC";
    private static String username = "root";
    private static String password = "root";
    @FXML
    private TextField Nationality;
    @FXML
    private TextField Sex;
    @FXML
    private TextField BirthYear;
    @FXML
    private TextField Phone;
    @FXML
    private Node Data;

    private String[] nations = new String[250];
    private String[] identity = new String[2];
    private String[] birth=new String[91];
    String email;
    //Metodo chiamato dal Main che permette un'identicazione dell'utente tramite email
    public void setEmailAddr(String e) { email = e; }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            txtBoxNationality();
            txtBoxSex();
            txtBoxBirthYear();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        TextFields.bindAutoCompletion(Nationality, nations); //Crea una casella di tipo autocomppilazione
        TextFields.bindAutoCompletion(Sex, identity);
        TextFields.bindAutoCompletion(BirthYear, birth);
    }

   // Metodo che permette di inizializzare la predizione delle nazionalità nella casella di testo
    public void txtBoxNationality() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT nationality FROM meetfly.countries");
        int i;
        i = 0;
        while (rs.next()) {
            nations[i] = rs.getString("nationality");
            i++;
        }
    }
    // Metodo che permette di inizializzare la predizione del sesso nella casella di testo
    public void txtBoxSex() {
        identity[0]="Male";
        identity[1]="Female";
    }
    // Metodo che permette di inizializzare la predizione degli anni  nella casella di testo
    public void txtBoxBirthYear(){
        int start_year=1920;
        for(int i=0;i<=90; i++){
            String y= Integer.toString(start_year);
            birth[i]= y;
            start_year++;
        }
    }

    /*Metodo che permette di salvare nel database i dati inseriti durante il set personal data
    con relativa attivazione di una label che indica l'avvenuto salvataggio
     */
    @FXML
    private void handleSaveData() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Data.setVisible(false);
        String nat = Nationality.getText();
        String sex = Sex.getText();
        String birthy = BirthYear.getText();
        String phone = Phone.getText();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("UPDATE users SET nationality=?, sex=?, birthyear=?, phone=? WHERE emailAddress = ?");
        pstmt.setString(1,nat);
        pstmt.setString(2,sex);
        pstmt.setString(3,birthy);
        pstmt.setString(4,phone);
        pstmt.setString(5, email);
        pstmt.executeUpdate();
        conn.close();
        Data.setVisible(true);
    }

}
