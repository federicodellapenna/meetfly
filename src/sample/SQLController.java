package sample;

/*
Classe per test del JDBS ma non utilizzata da MeetFly
 */
import java.sql.*;
import java.util.Scanner;

public class SQLController {
    private static String url="jdbc:mysql://localhost:3306/utente?useSSL=false&serverTimezone=UTC";
    private static String username="root";
    private static String password="root";
    public static void init(int arg) throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        //Scanner s=new Scanner();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st=conn.createStatement();
        ResultSet rs=st.executeQuery("SELECT * FROM amici");
        while (rs.next()){
            System.out.print(rs.getString("nome")+",");
            System.out.print(rs.getString("cognome")+",");
            System.out.print(rs.getString("telefono")+"\n");
        }
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM amici WHERE nome LIKE ?");
        pstmt.setString(1,"n%");
        rs=pstmt.executeQuery();
        while(rs.next()){
            System.out.print(rs.getInt("idamici")+",");
            System.out.print(rs.getString("nome")+",");
            System.out.print(rs.getString("cognome")+",");
            System.out.print(rs.getString("telefono")+"\n");
        }
        pstmt=conn.prepareStatement("INSERT INTO amici (nome,cognome,telefono) VALUES (?,?,?)");
        pstmt.setString(1,"n4");
        pstmt.setString(2,"c4");
        pstmt.setString(3,"t4");
        pstmt.executeUpdate();
        pstmt=conn.prepareStatement("UPDATE amici set nome=?,cognome=?,telefono=? where idamici=?");
        pstmt.setString(1,"n11");
        pstmt.setString(2,"c11");
        pstmt.setString(3,"t11");
        pstmt.setInt(4,1);
        pstmt.executeUpdate();
        pstmt=conn.prepareStatement("DELETE FROM amici WHERE idamici=?");
        pstmt.setInt(1,10);
        pstmt.executeUpdate();
        pstmt=conn.prepareStatement("DELETE FROM amici WHERE cognome=?");
        pstmt.setString(1,"c5");
        pstmt.executeUpdate();
        conn.close();
    }
    public void addUserOnDB(String usr,String pwd,String nm,String sn,String ph)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("INSERT INTO users (name,surname,emailAddress,password,phone) VALUES (?,?,?,?,?)");
        pstmt.setString(1,nm);
        pstmt.setString(2,sn);
        pstmt.setString(3,usr);
        pstmt.setString(4,pwd);
        pstmt.setString(5,ph);
        pstmt.executeUpdate();

        conn.close();

    }
}
