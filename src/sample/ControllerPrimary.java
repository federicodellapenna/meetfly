package sample;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.Vector;

import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import org.controlsfx.control.textfield.TextFields;


//Classe che racchiude tutti i controlli della Primary window


public class ControllerPrimary extends Main implements Initializable {
    private static String url = "jdbc:mysql://localhost:3306/meetfly?useSSL=false&serverTimezone=UTC";
    private static String username = "root";
    private static String password = "root";
    Main cp = new Main();
    String email;
    @FXML
    private ProgressIndicator progress;
    @FXML
    private Slider sliderSearch;
    @FXML
    private Pane root;
    @FXML
    private TextField Destination;
    @FXML
    private TextField Flight;
    @FXML
    private TextField City;
    @FXML
    private TextField CityCode;
    @FXML
    private TextField Street;
    @FXML
    private TextField StreetNumber;
    private String[] airports = new String[3593];
    private String[] rotte = new String[68000];
    @FXML
    private TableView<Users> UserTable;
    @FXML
    private TableColumn<Users, String> emailColumn;
    @FXML
    private TableColumn<Users, String> airportColumn;
    @FXML
    private TableColumn<Users, String> flightColumn;
    @FXML
    private TableColumn<Users, String> cityColumn;
    @FXML
    private TableColumn<Users, String> cityCodeColumn;
    @FXML
    private TableColumn<Users, String> streetColumn;
    @FXML
    private TableColumn<Users, String> numberColumn;
    @FXML
    private CheckBox streetFilter;
    @FXML
    private  CheckBox streetNumberFilter;
    @FXML private Rectangle rectHelp;
    @FXML private Text helpTxt;
    @FXML
    private CheckBox flightFilter;
    private Vector<String> unames=new Vector<String>();

    //Metodo chiamato dal Main che permette un'identicazione dell'utente tramite email
    public void setEmailAddr(String e) { email = e; }

    //Metodo che permette l'apertura della finestra per il change password da parte dell'utente(Menu)
    @FXML
    private void handleSet() {
        cp.startChangePwd();
    }

    //Metodo che permette l'apertura della finestra per il logout (Menu)
    @FXML
    private void handleLogout(){startLogout(email);}

    //Metodo che permette l'apertura della finestra per l'inserimento di ulteriori dati personali (Menu)
    @FXML
    private void handleInsertData(){startInsertData(email);}


    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */

    /*Metodo che inizializza la tabella dei risultati e tutte le caselle che offrono un suggerimento
       quali aereporto e codice volo
     */

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            txtBoxAirports();
            txtBoxFlights();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        TextFields.bindAutoCompletion(Destination, airports);
        TextFields.bindAutoCompletion(Flight, rotte);

        //Inizialization table
        emailColumn.setCellValueFactory(cellData -> cellData.getValue().emailProperty());
        airportColumn.setCellValueFactory(cellData -> cellData.getValue().airportProperty());
        flightColumn.setCellValueFactory(cellData -> cellData.getValue().flightlProperty());
        cityColumn.setCellValueFactory(cellData -> cellData.getValue().cityProperty());
        cityCodeColumn.setCellValueFactory(cellData -> cellData.getValue().cityCodeProperty());
        streetColumn.setCellValueFactory(cellData -> cellData.getValue().streetProperty());
        numberColumn.setCellValueFactory(cellData ->cellData.getValue().numberProperty());
        emailColumn=new TableColumn("Email");
        airportColumn=new TableColumn("Airport");
        flightColumn=new TableColumn("Flight");
        cityColumn=new TableColumn("City");
        cityCodeColumn=new TableColumn("CityCode");
        streetColumn= new TableColumn("Street");
        numberColumn=new TableColumn("Number");


    }

    // Metodo che permette di inizializzare la predizione degli aereoporti nella casella di testo
    public void txtBoxAirports() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM meetfly.iata_airport_codes");
        int i;
        i = 0;
        while (rs.next()) {
            airports[i] = rs.getString("airport") + ", " + rs.getString("code");
            i++;
        }
    }

    // Metodo che permette di inizializzare la predizione dei voli nella casella di testo
    public void txtBoxFlights() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM meetfly.rotte");
        int i;
        i = 0;
        while (rs.next()) {
            rotte[i] = rs.getString("lineaAerea") + "-" + rs.getString("idCompagniaAerea") + ", from " + rs.getString("aereoportoOrigine") + " to " + rs.getString("aereoportoDestinazione");
            i++;
        }
    }

    //gestisce l'attivazione/disattivazione del banner di help
    @FXML
    public void handleHelpPrimary(){
        if(!helpTxt.isVisible()&&!rectHelp.isVisible()) {
            helpTxt.setVisible(true);
            rectHelp.setVisible(true);
        }
        else{
            helpTxt.setVisible(false);
            rectHelp.setVisible(false);
        }
    }

    /*Metodo che si occupa della ricerca degli utenti con città che distano 10,20,30 e 40 km
       dalla città inserita dall'utente che compie la ricerca ove le distanze sono selezionate dallo stesso
       tramite uno slider. La ricerca si basa in ogni caso sui dati inseriti nella stessa giornata.
     */
    @FXML
    public void handleSearchNeighbours() throws ClassNotFoundException,IllegalAccessException,InstantiationException,SQLException, IOException {
        progress.setVisible(true);
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st = conn.createStatement();
        ObservableList<Users> data;
        UserTable.getItems().clear();
        String destination = Destination.getText();
        String flight = Flight.getText();
        String city = City.getText();
        //Ricerca effettuata filtrando in base al codice di volo
        if(flightFilter.isSelected()) {
            PreparedStatement pstmt = conn.prepareStatement("UPDATE users SET airportCode=?,flightCode=?,city=? WHERE emailAddress=?");
            pstmt.setString(1, destination);
            pstmt.setString(2, flight);
            pstmt.setString(3, city);
            pstmt.setString(4, email);//Link with DB on Sign Up successful, method is into Controller (not SQLController).
            pstmt.executeUpdate();
            PreparedStatement pstmt4 = conn.prepareStatement("UPDATE users SET dateInsert=current_date() WHERE emailAddress=?");
            pstmt4.setString(1, email);
            pstmt4.executeUpdate();
            progress.setProgress(50.0);
            PreparedStatement pstmt2 = conn.prepareStatement("SELECT city FROM users WHERE airportCode=? AND flightCode=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY ");
            pstmt2.setString(1, destination);
            pstmt2.setString(2, flight);
            ResultSet rs = pstmt2.executeQuery();
            String[] cities = new String[500];
            progress.setProgress(75.0);
            int i = 0;
            while (rs.next()) {
                //costruisce un vettore (C/C++ style) che raccoglie tutti i nomi delle città degli utenti.
                //Ciò è utile per il ciclo di ricerca in base alla distanza.
                if (rs.getString("city") != null) {
                    cities[i] = rs.getString("city");
                    i++;
                }

            }
            for (int k = 0; k < i; k++) {
                //condizione nel ciclo che verifica le distanze tra città, viene esclusa la città stessa
                //in quanto per quella è consigliata la ricerca standard.
                if (distance(city, cities[k]) <= sliderSearch.getValue() && distance(city,cities[k])>0) {
                    //System.out.println(cities[k]);
                    PreparedStatement pstmt3 = conn.prepareStatement("SELECT * FROM users WHERE city=?");
                    pstmt3.setString(1, cities[k]);
                    ResultSet rs2 = pstmt3.executeQuery();
                    unames.clear(); //pulizia del vector dei nickname (utile per la visualizzazione dei dati personali)ad ogni nuova ricerca
                    //Si provvede all'inserimento in tabella dei risultati
                    while (rs2.next()) {
                        data = UserTable.getItems();
                        unames.add(rs2.getString("username"));
                        emailColumn.setText(rs2.getString("username"));
                        airportColumn.setText(rs2.getString("airportCode"));
                        flightColumn.setText(rs2.getString("flightCode"));
                        cityColumn.setText(rs2.getString("city"));
                        cityCodeColumn.setText(rs2.getString("cityCode"));
                        streetColumn.setText(rs2.getString("street"));
                        numberColumn.setText(rs2.getString("number"));
                        data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                    }
                }
            }
            progress.setProgress(100.0);
            //La ricerca viene compiuta non filtrando in base al codice del volo
        }else if(!flightFilter.isSelected()){
            PreparedStatement pstmt = conn.prepareStatement("UPDATE users SET airportCode=?,flightCode=?,city=? WHERE emailAddress=?");
            pstmt.setString(1, destination);
            pstmt.setString(2, flight);
            pstmt.setString(3, city);
            pstmt.setString(4, email);//Link with DB on Sign Up successful, method is into Controller (not SQLController).
            pstmt.executeUpdate();
            PreparedStatement pstmt4 = conn.prepareStatement("UPDATE users SET dateInsert=current_date() WHERE emailAddress=?");
            pstmt4.setString(1, email);
            pstmt4.executeUpdate();
            progress.setProgress(50.0);
            PreparedStatement pstmt2 = conn.prepareStatement("SELECT city FROM users WHERE airportCode=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY ");
            pstmt2.setString(1, destination);
            ResultSet rs = pstmt2.executeQuery();
            String[] cities = new String[500];
            progress.setProgress(75.0);
            int i = 0;
            while (rs.next()) {

                if (rs.getString("city") != null) {
                    cities[i] = rs.getString("city");
                    i++;
                }

            }
            for (int k = 0; k < i; k++) {
                if (distance(city, cities[k]) <= sliderSearch.getValue() && distance(city,cities[k])>0) {
                    //System.out.println(cities[k]);
                    PreparedStatement pstmt3 = conn.prepareStatement("SELECT * FROM users WHERE city=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY ");
                    pstmt3.setString(1, cities[k]);
                    ResultSet rs2 = pstmt3.executeQuery();
                    unames.clear();
                    while (rs2.next()) {
                        unames.add(rs2.getString("username"));
                        data = UserTable.getItems();
                        emailColumn.setText(rs2.getString("username"));
                        airportColumn.setText(rs2.getString("airportCode"));
                        flightColumn.setText(rs2.getString("flightCode"));
                        cityColumn.setText(rs2.getString("city"));
                        cityCodeColumn.setText(rs2.getString("cityCode"));
                        streetColumn.setText(rs2.getString("street"));
                        numberColumn.setText(rs2.getString("number"));
                        data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                    }
                }
            }
            progress.setProgress(100.0);
        }
    }

      /*Metodo che si occupa della ricerca degli utenti con la stessa città inserita dall'utente.
      La ricerca si basa in ogni caso sui dati inseriti nella stessa giornata.
     */
    @FXML
    public void handleSearch() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        progress.setVisible(true);
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st = conn.createStatement();
        ObservableList<Users> data;
        UserTable.getItems().clear();
        String destination = Destination.getText();
        String flight = Flight.getText();
        String city = City.getText();
        String cityCode= CityCode.getText();
        String street = Street.getText();
        String number = StreetNumber.getText();
        //Ricerca compiuta in base al volo
        if (flightFilter.isSelected()) {
            PreparedStatement pstmt = conn.prepareStatement("UPDATE users SET airportCode=?,flightCode=?,city=?,cityCode=?,street=?,number=? WHERE emailAddress=?");
            pstmt.setString(1, destination);
            pstmt.setString(2, flight);
            pstmt.setString(3, city);
            pstmt.setString(4, cityCode);
            pstmt.setString(5, street);
            pstmt.setString(6, number);
            pstmt.setString(7, email);
            pstmt.executeUpdate();
            PreparedStatement pstmt3 = conn.prepareStatement("UPDATE users SET dateInsert=current_date() WHERE emailAddress=?");
            pstmt3.setString(1, email);
            pstmt3.executeUpdate();
            //Ricerca compiuta filtrando per via e numero civico
            if (streetFilter.isSelected() && streetNumberFilter.isSelected()) {
                PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM users WHERE airportCode=? AND flightCode=? AND city=? AND cityCode=? AND street=? AND number=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY");
                pstmt2.setString(1, destination);
                pstmt2.setString(2, flight);
                pstmt2.setString(3, city);
                pstmt2.setString(4, cityCode);
                pstmt2.setString(5, street);
                pstmt2.setString(6, number);
                ResultSet rs = pstmt2.executeQuery();
                unames.clear();
                while (rs.next()) {
                    unames.add(rs.getString("username"));
                    data = UserTable.getItems();
                    emailColumn.setText(rs.getString("username"));
                    airportColumn.setText(rs.getString("airportCode"));
                    flightColumn.setText(rs.getString("flightCode"));
                    cityColumn.setText(rs.getString("city"));
                    cityCodeColumn.setText(rs.getString("cityCode"));
                    streetColumn.setText(rs.getString("street"));
                    numberColumn.setText(rs.getString("number"));
                    data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                }
                progress.setProgress(100.0);
                //Ricerca effettuata in base al solo filtro di via e non numero civico
            } else if (streetFilter.isSelected() && !streetNumberFilter.isSelected()) {
                PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM users WHERE airportCode=? AND flightCode=? AND city=? AND cityCode=? AND street=?AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY");
                pstmt2.setString(1, destination);
                pstmt2.setString(2, flight);
                pstmt2.setString(3, city);
                pstmt2.setString(4, cityCode);
                pstmt2.setString(5, street);
                ResultSet rs = pstmt2.executeQuery();
                unames.clear();
                while (rs.next()) {
                    unames.add(rs.getString("username"));
                    data = UserTable.getItems();
                    emailColumn.setText(rs.getString("username"));
                    airportColumn.setText(rs.getString("airportCode"));
                    flightColumn.setText(rs.getString("flightCode"));
                    cityColumn.setText(rs.getString("city"));
                    cityCodeColumn.setText(rs.getString("cityCode"));
                    streetColumn.setText(rs.getString("street"));
                    numberColumn.setText(rs.getString("number"));
                    data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                }
                progress.setProgress(100.0);
                //Ricerca effettuata non filtrando  nè in base alla via  che al numero civico
            } else if (!streetFilter.isSelected() && !streetNumberFilter.isSelected()) {
                PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM users WHERE airportCode=? AND flightCode=? AND city=? AND cityCode=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY");
                pstmt2.setString(1, destination);
                pstmt2.setString(2, flight);
                pstmt2.setString(3, city);
                pstmt2.setString(4, cityCode);
                ResultSet rs = pstmt2.executeQuery();
                unames.clear();
                while (rs.next()) {
                     unames.add(rs.getString("username"));
                    data = UserTable.getItems();
                    emailColumn.setText(rs.getString("username"));
                    airportColumn.setText(rs.getString("airportCode"));
                    flightColumn.setText(rs.getString("flightCode"));
                    cityColumn.setText(rs.getString("city"));
                    cityCodeColumn.setText(rs.getString("cityCode"));
                    streetColumn.setText(rs.getString("street"));
                    numberColumn.setText(rs.getString("number"));

                    data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                }
                progress.setProgress(100.0);
            }
            //Ricerca effettuata non filtrando il codice volo
        }else if (!flightFilter.isSelected()){
            PreparedStatement pstmt = conn.prepareStatement("UPDATE users SET airportCode=?,flightCode=?,city=?,cityCode=?,street=?,number=? WHERE emailAddress=?");
            pstmt.setString(1, destination);
            pstmt.setString(2, flight);
            pstmt.setString(3, city);
            pstmt.setString(4, cityCode);
            pstmt.setString(5, street);
            pstmt.setString(6, number);
            pstmt.setString(7, email);
            pstmt.executeUpdate();
            PreparedStatement pstmt3 = conn.prepareStatement("UPDATE users SET dateInsert=current_date() WHERE emailAddress=?");
            pstmt3.setString(1, email);
            pstmt3.executeUpdate();
            //Ricerca compiuta filtrando per via e numero civico
            if (streetFilter.isSelected() && streetNumberFilter.isSelected()) {
                PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM users WHERE airportCode=? AND city=? AND cityCode=? AND street=? AND number=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY");
                pstmt2.setString(1, destination);
                pstmt2.setString(2, city);
                pstmt2.setString(3, cityCode);
                pstmt2.setString(4, street);
                pstmt2.setString(5, number);
                ResultSet rs = pstmt2.executeQuery();
                unames.clear();
                while (rs.next()) {
                    unames.add(rs.getString("username"));
                    data = UserTable.getItems();
                    emailColumn.setText(rs.getString("username"));
                    airportColumn.setText(rs.getString("airportCode"));
                    flightColumn.setText(rs.getString("flightCode"));
                    cityColumn.setText(rs.getString("city"));
                    cityCodeColumn.setText(rs.getString("cityCode"));
                    streetColumn.setText(rs.getString("street"));
                    numberColumn.setText(rs.getString("number"));
                    data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                }
                progress.setProgress(100.0);
                //Ricerca effettuata in base al solo filtro di via e non numero civico
            } else if (streetFilter.isSelected() && !streetNumberFilter.isSelected()) {
                PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM users WHERE airportCode=? AND city=? AND cityCode=? AND street=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY");
                pstmt2.setString(1, destination);
                pstmt2.setString(2, city);
                pstmt2.setString(3, cityCode);
                pstmt2.setString(4, street);
                ResultSet rs = pstmt2.executeQuery();
                unames.clear();
                while (rs.next()) {
                    unames.add(rs.getString("username"));
                    data = UserTable.getItems();
                    emailColumn.setText(rs.getString("username"));
                    airportColumn.setText(rs.getString("airportCode"));
                    flightColumn.setText(rs.getString("flightCode"));
                    cityColumn.setText(rs.getString("city"));
                    cityCodeColumn.setText(rs.getString("cityCode"));
                    streetColumn.setText(rs.getString("street"));
                    numberColumn.setText(rs.getString("number"));
                    data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                }
                progress.setProgress(100.0);
                //Ricerca effettuata non filtrando  nè in base alla via  che al numero civico
            } else if (!streetFilter.isSelected() && !streetNumberFilter.isSelected()) {
                PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM users WHERE airportCode=? AND city=? AND cityCode=? AND dateInsert>CURRENT_DATE-INTERVAL 1 DAY ");
                pstmt2.setString(1, destination);
                pstmt2.setString(2, city);
                pstmt2.setString(3, cityCode);
                ResultSet rs = pstmt2.executeQuery();
                unames.clear();
                while (rs.next()) {
                   unames.add(rs.getString("username"));
                    data = UserTable.getItems();
                    emailColumn.setText(rs.getString("username"));
                    airportColumn.setText(rs.getString("airportCode"));
                    flightColumn.setText(rs.getString("flightCode"));
                    cityColumn.setText(rs.getString("city"));
                    cityCodeColumn.setText(rs.getString("cityCode"));
                    streetColumn.setText(rs.getString("street"));
                    numberColumn.setText(rs.getString("number"));

                    data.add(new Users(emailColumn.getText(), airportColumn.getText(), flightColumn.getText(), cityColumn.getText(), cityCodeColumn.getText(), streetColumn.getText(), numberColumn.getText()));
                }
                progress.setProgress(100.0);
            }

        }


        conn.close();

    }

    /*
    Si occupa dell'avvio del chat client, estraendo il nickname selezionato ed avviando la chat secondo i criteri
    (mittente,destinatario) passati come parametri.
     */
    @FXML
    public void handleChat() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        String mitt="";
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st = conn.createStatement();
        PreparedStatement pstmt10=conn.prepareStatement("SELECT username FROM users WHERE emailAddress=?");
        pstmt10.setString(1,email);
        ResultSet rs10=pstmt10.executeQuery();
        while(rs10.next()){
            mitt= (String)rs10.getString("username");
        }
        ObservableList<Users> usersObservableList;
        usersObservableList=UserTable.getSelectionModel().getSelectedItems();
        System.out.println(usersObservableList.get(0).getEmail());
        String dest=  (String) usersObservableList.get(0).getEmail();

        startChat(mitt,dest);

    }

    /*
    Si occupa del calcolo della distanza tra una città e l'altra tramite API MapQuest.
    Estrae direttamente la distanza dal JSON di output, lo converte in double e lo restituisce.
     */
    public double distance(String from, String to) throws IOException {
        double dist;
        String key="9ToK5k1z0SBisv1ouIkAP6QkogGBCspQ";
        String from2=from.replace(" ","+");
        String from3=from2.replace("'","%20");
        String to2=to.replace(" ", "+");
        String to3=to2.replace("'","%20");
        URL url = new URL("https://www.mapquestapi.com/directions/v2/route?key="+key+"&from=" + from3 + "&to=" + to3 + "&outFormat=json&ambiguities=ignore&routeType=fastest&doReverseGeocode=false&enhancedNarrative=false&avoidTimedConditions=false");
        Scanner s = new Scanner(url.openStream());
        s.findInLine("distance");
        String input = s.nextLine();
        String partBeforeFullStop = input.split("\\,")[0];
        String s2 = partBeforeFullStop.substring(2);
        //System.out.println(s2); debug
        dist=Double.parseDouble(s2);
        return dist;
    }
    @FXML
    //disattiva la progress bar quando necessario
    public void handleProgress(){
          progress.setVisible(false);
    }

    @FXML
    //Seleziona l'utente in tabella ed apre la finestra di Personal Data
    public void handleAddData(){
        ObservableList<Users> usersObservableList;
        usersObservableList=UserTable.getSelectionModel().getSelectedItems();
        String dest=  (String) usersObservableList.get(0).getEmail();
        startData(unames,dest);}


}

