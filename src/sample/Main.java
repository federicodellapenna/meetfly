package sample;
/*
Questa classe gestisce l'apertura delle finestre principali, inizializzando i file del CSS
 dunque l'avviamento delle interfacce grafiche
 */


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Vector;


public class Main extends Application {
    private Stage primaryStage;
    private Stage signInStage;
    private Stage loginStage;
    private Stage interfaceStage;
    private Stage changePwdStage;
    private Stage LogoutStage;
    private Stage SafeLogoutStage;
    private Stage ChatStage;
    private Stage InsertDataStage;
    private Pane rootLayout;
    private Pane signIn;
    private Stage DataStage;
    public static  Scene MainloginScene;
    public static Scene MainSignUpScene;
    public static Scene MainLogoutScene;
    public static Scene MainPrimaryScene;
    public static Scene MainSafeScene;




    //Apertura interfaccia home
    @Override
    public void start(Stage primaryStage){
        this.primaryStage=primaryStage;
        this.primaryStage.setTitle("MeetFly - Login or Sign Up");
        initRootLayout();
        this.primaryStage.getIcons().add(new Image("file:src/sample/sfondo.png"));
        this.primaryStage.show();
    }


    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("sample.fxml"));
            rootLayout = (Pane) loader.load();
            rootLayout.setId("pane");
            Scene scene = new Scene(rootLayout);
            scene.getStylesheets().addAll(this.getClass().getResource("style.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.setOnCloseRequest(event -> {
                event.consume();
                handleExit();
            });
            Controller controller = loader.getController();
            controller.setMainApp(this);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    //Apertura interfaccia di registrazione al servizio
    public void startSignIn(){
        try {
            signInStage=new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("signin.fxml"));
            Pane signIn = (Pane) loader.load();
            signInStage.setTitle("MeetFly - Sign Up");
            signInStage.initModality((Modality.APPLICATION_MODAL));
            signInStage.initOwner(primaryStage);
            MainSignUpScene = new Scene(signIn);
            signInStage.setScene(MainSignUpScene);
            ControllerSignUp controller = loader.getController();
            signInStage.show();

        }catch (IOException e){
            e.printStackTrace();

        }


    }

    //Apertura di interfaccia di login
    public void startLogin()  {
        try{
            loginStage=new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("login.fxml"));
            Pane login = (Pane) loader.load();
            loginStage.setTitle("MeetFly - Sign In");
            loginStage.initModality((Modality.APPLICATION_MODAL));
            loginStage.initOwner(primaryStage);
            MainloginScene= new Scene(login);
            loginStage.setScene(MainloginScene);

            ControllerLog controller = loader.getController();
            loginStage.show();




        }catch (IOException e){
            e.printStackTrace();

        }


    }

    //Interfaccia di uscita dal servizio
    public void handleExit() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Are you sure?");
        alert.setHeaderText("Exit");
        alert.setContentText("Quit from MeetFly");


        ButtonType buttonTypeOne = new ButtonType("Yes");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne){
            System.exit(0);
        }

    }

    //Alert di tipo warning per il logout nella primariy interface
    public void handleOut() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Pay attention");
        alert.setHeaderText("First Logout");
        alert.setContentText("You have to go to Menu and Logout");

        ButtonType buttonTypeCancel = new ButtonType("OK", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();

        }



     //Apertura della primary interface (interfaccia di ricerca)
    public void startPrimaryInterface(String email) {
        try {
             interfaceStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("PrimaryInterface.fxml"));
            Pane PrimaryInterface = (Pane) loader.load();
            interfaceStage.setTitle("MeetFly - Welcome "+ email);
            interfaceStage.initModality((Modality.APPLICATION_MODAL));
            interfaceStage.initOwner(primaryStage);
            MainPrimaryScene= new Scene(PrimaryInterface);
            interfaceStage.setScene(MainPrimaryScene);
            ControllerPrimary controller = loader.getController();
            controller.setEmailAddr(email);
            interfaceStage.show();
            interfaceStage.setOnCloseRequest(event -> {
                event.consume();
                handleOut();
            });


        } catch (IOException e) {
            e.printStackTrace();

        }

    }
    //Apertura della finestra nella primary interface che permette l'aggiornamento password
    public void startChangePwd() {
        try {
            changePwdStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("UpdatePwd.fxml"));
            Pane UpdatePwd = (Pane) loader.load();
            changePwdStage.setTitle("Update Password");
            changePwdStage.initModality((Modality.APPLICATION_MODAL));
            changePwdStage.initOwner(primaryStage);
            Scene scene = new Scene(UpdatePwd);
            changePwdStage.setScene(scene);
            ControllerPwd controller = loader.getController();
            changePwdStage.show();


        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    //Apertura della finestra di logout nella primary interface
    public void startLogout(String email) {
        try {
            LogoutStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("Logout.fxml"));
            Pane Logout = (Pane) loader.load();
            LogoutStage.setTitle("Logout Meetfly");
            LogoutStage.initModality((Modality.APPLICATION_MODAL));
            LogoutStage.initOwner(primaryStage);
            MainLogoutScene = new Scene(Logout);
            LogoutStage.setScene(MainLogoutScene);
            ControllerLogout controller = loader.getController();
            controller.setEmailAddr(email);
            LogoutStage.show();


        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    //Apertura della finestra per inserimento dati personali nella primary interface
    public void startInsertData(String email) {
        try {
            InsertDataStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("PersonalData.fxml"));
            Pane PersonalData = (Pane) loader.load();
            InsertDataStage.setTitle("Personal Data");
            InsertDataStage.initModality((Modality.APPLICATION_MODAL));
            InsertDataStage.initOwner(primaryStage);
            Scene scene = new Scene(PersonalData);
            InsertDataStage.setScene(scene);
            ControllerData controller = loader.getController();
            controller.setEmailAddr(email);
            InsertDataStage.show();


        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    //Apertura della finestra, dopo aver acconsentito al logout, per il salvataggio dei dati
    public void startSafeLogout(String email) {
        try {
            SafeLogoutStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("SafeLogout.fxml"));
            Pane SafeLogout = (Pane) loader.load();
            SafeLogoutStage.setTitle("Safe Logout Meetfly");
            SafeLogoutStage.initModality((Modality.APPLICATION_MODAL));
            SafeLogoutStage.initOwner(primaryStage);
            MainSafeScene= new Scene(SafeLogout);
            SafeLogoutStage.setScene(MainSafeScene);
            ControllerSafe controller = loader.getController();
            controller.setEmailAddr(email);
            SafeLogoutStage.show();


        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    //Apertura schermata di chat
    public void startChat(String mitt,String dest) {
        try {
            ChatStage= new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("Chat.fxml"));
            Pane Chat = (Pane) loader.load();
            ChatStage.setTitle("Let's start chatting");
            ChatStage.initModality((Modality.APPLICATION_MODAL));
            ChatStage.initOwner(primaryStage);
            Scene scene= new Scene(Chat);
            ChatStage.setScene(scene);
            ControllerChat controller=loader.getController();
            controller.setDes(dest);
            controller.setMit(mitt);
            ChatStage.show();


        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    //Apertura finestra di visualizzazione dati aggiuntivi
    public void startData(Vector<String> u, String d) {
        try {
            DataStage= new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("Data.fxml"));
            Pane Chat = (Pane) loader.load();
            DataStage.setTitle("View more about Meetflyers");
            DataStage.initModality((Modality.APPLICATION_MODAL));
            DataStage.initOwner(primaryStage);
            Scene scene= new Scene(Chat);
            DataStage.setScene(scene);
            ControllerAddData controller=loader.getController();
            controller.setUnames(u,d);
            DataStage.show();


        } catch (IOException e) {
            e.printStackTrace();

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }




    public static void main(String[] args) {
        launch(args);
    }

}