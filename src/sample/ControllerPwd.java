package sample;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.sql.*;

public class ControllerPwd {
    private static String url = "jdbc:mysql://localhost:3306/meetfly?useSSL=false&serverTimezone=UTC";
    @FXML private TextField emailBox;
    @FXML private PasswordField oldPwdBox;
    @FXML private PasswordField newPwdBox;
    @FXML private PasswordField repPwdBox;
    @FXML private Node update;
    @FXML private Node error;
    @FXML private Node match;
    @FXML private Node lenght;

    @FXML
    //Applica l'aggiornamento password
    private void handleApply() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        String email=emailBox.getText();
        String oldPwd=oldPwdBox.getText();
        String newPwd=newPwdBox.getText();
        String repPwd=repPwdBox.getText();
        //Caso favorevole
        if(pwdCheckDB(email, oldPwd) && newPwd.equals(repPwd) && !email.isEmpty() && !oldPwd.isEmpty() && !newPwd.isEmpty() && !repPwd.isEmpty() && pwdCheck(newPwd)) {
            updatePwdDB(email, newPwd);
            update.setVisible(true);
            error.setVisible(false);
            match.setVisible(false);
            lenght.setVisible(false);

        }//se uno o più campi sono vuoti
        else if(email.isEmpty() || oldPwd.isEmpty() || newPwd.isEmpty() || repPwd.isEmpty()){
            update.setVisible(false);
            error.setVisible(true);
            match.setVisible(false);
            lenght.setVisible(false);
        }//se le due password non coincidono
        else if(!newPwd.equals(repPwd)){
            update.setVisible(false);
            error.setVisible(false);
            match.setVisible(true);
            lenght.setVisible(false);

        }//se la password non rispetta la lunghezza imposta dal servizio
        else if(!pwdCheck(newPwd)){
            update.setVisible(false);
            error.setVisible(false);
            match.setVisible(false);
            lenght.setVisible(true);

        }
    }

    //aggiorna il campo password nel DB
    public void updatePwdDB(String usr,String np)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("UPDATE users SET password = ? WHERE emailAddress = ?");
        pstmt.setString(1,np);
        pstmt.setString(2,usr);
        pstmt.executeUpdate();
        conn.close();

    }

    //verifica tramite email e password old se nel DB esistono match
    public boolean pwdCheckDB(String usr, String pwd)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM meetfly.users WHERE  emailAddress LIKE ? AND password LIKE ?");
        pstmt.setString(1,usr);
        pstmt.setString(2, pwd);
        ResultSet rs= pstmt.executeQuery();
        boolean result = rs.next();
        conn.close();
        return result;

    }

    //controllo di check lunghezza password
    public boolean pwdCheck(String password){
        if(password.length()<=5){
            return (false);
        }else return (true);
    }
    @FXML //gestisce la disattivazione della label di errore se si tenta una reimmissione
    public void handleActionTxtBox(){
        error.setVisible(false);
        match.setVisible(false);
        lenght.setVisible(false);
    }

}
