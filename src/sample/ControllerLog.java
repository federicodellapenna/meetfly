package sample;
/*
Gestisce le azioni sulla finestra di login.
Tutti i Button, TextBox, ecc. interagiscono con gli oggetti di questa classe.
 */
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import javafx.stage.Window;

import javax.mail.MessagingException;
import java.io.RandomAccessFile;
import java.sql.*;
import java.util.Optional;
import java.util.Random;

public class ControllerLog extends Main{
        private String recPass;
        private EmailUtility e;
        private static String url="jdbc:mysql://localhost:3306/meetfly?useSSL=false&serverTimezone=UTC";
        Main pi = new Main();
        @FXML private TextField emailAddressBox;
        @FXML private PasswordField PwdBox;
        @FXML private Node ForPwdLabel;
        @FXML private Node WrongCred;
        @FXML

        //Metodo che gestisce il button del login effettuando il controllo sui campi
        // attivando eventuali etichette di avviso di email o password errata
        private void handleLoggedIn() throws MessagingException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
            String email=emailAddressBox.getText();
            String Pwd=PwdBox.getText();
            if(credCheckDB(email,Pwd)){
                WrongCred.setVisible(false);
                startPrimaryInterface(email);
                MainloginScene.getWindow().hide(); //consente la chiusura della finestra di login una volta effettuato l'accesso al servizio


            }else{
                WrongCred.setVisible(true); //label di errore credenziali
            }

        }


        @FXML //gestisce la disattivazione della label wrong cred se si tenta una reimmissione
        public void handleActionTxtBox(){
            WrongCred.setVisible(false);
        }
    /*Gestisce il bottone per password dimenticata. Questo metodo permette di poter accedere al servizio
    tramite una password randomica inviata dal MeetFly staff tramite email.
    Ad invio avvenuto una label verrà visualizzata.
    */
       @FXML
       public void handleForgotPass() throws MessagingException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
            WrongCred.setVisible(false);
            String email= emailAddressBox.getText();
            recPass=generatePassword();
            updatePassDB(email,recPass);
            String recMessage="Hi!\nWe have reset the password for you.\n"+recPass+" is the new one.\nEnjoy Meetfly!";
            e.sendEmail("smtp.libero.it", "25", "meetfly2019@libero.it", "noreplymeetfly",
                    email, "Recover password", recMessage);
             ForPwdLabel.setVisible(true);

       }
       //Genera una password alfabetica randomica
    public String generatePassword() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        //System.out.println(generatedString); debug
        return generatedString;
    }

    //Metodo che aggiorna la password all'interno del database ove sono conservati i dati richiesti dell'utente
    public void updatePassDB(String usr,String np)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("UPDATE users SET password = ? WHERE emailAddress = ?");
        pstmt.setString(1,np);
        pstmt.setString(2,usr);
        pstmt.executeUpdate();
        conn.close();

    }
    //Metodo che controlla le credenziali di login dell'user-> true in caso siano già inserite, false altrimenti
    public boolean credCheckDB(String usr, String pwd)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM meetfly.users WHERE  emailAddress LIKE ? AND password LIKE ?");
        pstmt.setString(1,usr);
        pstmt.setString(2, pwd);
        ResultSet rs= pstmt.executeQuery();
        boolean result = rs.next();
        conn.close();
        return result;

    }
}
