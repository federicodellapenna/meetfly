package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.activation.MailcapCommandMap;
/*
Questa classe si occupa dei controlli della prima schermata fornita all'utente.
Gestisce, quindi, l'help della schermata iniziale, consente l'apertura della finestra di Login e Sign-up.
 */
public class Controller {
    Main sw=new Main();
    Main tw=new Main();
    private Main mainApp;
    @FXML private Ellipse helpTxtEllipse;
    @FXML private Text helpTxtWelcome;
    @FXML
    private void handleSignIn(){
        sw.startSignIn();
    }
    public void setMainApp(Main m) {
        this.mainApp = m;
    }
    @FXML
    private void handleLogin()  {

        tw.startLogin();
    }
    private void handleExit() {
        tw.handleExit();

    }
    @FXML
    private void handleHelp(){
        if(!helpTxtWelcome.isVisible()&&!helpTxtEllipse.isVisible()) {
            helpTxtEllipse.setVisible(true);
            helpTxtWelcome.setVisible(true);
        }
        else{
            helpTxtEllipse.setVisible(false);
            helpTxtWelcome.setVisible(false);
        }
    }
}
