package sample;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import javax.mail.*;
import javax.mail.search.FlagTerm;
import java.util.Properties;

/*
Questa classe gestisce tutti i controlli possibili per il client di chat, con metodi per il setting dei nickname e
della lettura dei messaggi in arrivo.
L'invio dei messaggi è invece deputato alla classe EmailUtility.
 */

public class ControllerChat {
        String mit;//nick del mittente
        String des;//nick del destinatario
        @FXML
        private TextField message;//casella del messaggio
        @FXML
        private TextArea txtarea;//caselle dei messaggi
        private EmailUtility e;
        @FXML
        private Node Done;
       public void setMit(String mitt){ //imposta il nickname del mittente
           mit=mitt;
       }
    public void setDes(String dest){ //imposta il nickname del destinatario
        des=dest;
    }
        @FXML
        public void handleMessage() throws Exception {
            String lastmsg = mit+": " + message.getText() + "\n";//il messaggio che verrà inviato, composto da Nickname: messaggio
            message.clear();//pulisce la casella di scrittura del messaggio appena si da Enter
            txtarea.appendText(lastmsg);//inserisce il messaggio nello stream dei messaggi nella TextArea.
            //String msg=c.getMsg();
            e.sendEmail("smtp.libero.it", "25", "meetfly2019@libero.it", "noreplymeetfly",
                    "meetfly2019@libero.it", "MTFMSG:" + mit + "-" + des, lastmsg);
            //invia il messaggio al server tramite protocollo smtp.

        }

        /*
        Controlla eventuali nuovi messaggi tramite protocollo pop3, inserendo nel buffer dei nuovi messaggi
        eventuali aggiornamenti dall'altro utente. Di volta in volta, tramite protocollo se ne prevede la cancellazione
        dal server.
         */

        @FXML
        public void mailCheck() throws Exception {
            Done.setVisible(false);

            Session session = Session.getDefaultInstance(new Properties());
            Store store = session.getStore("pop3s");
            store.connect("pop3.libero.it", 995, "meetfly2019@libero.it", "noreplymeetfly");
            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_WRITE);
            // Fetch unseen messages from inbox folder
            Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.DELETED), false));
            for (Message message : messages) {
                if (message.getSubject().contains("MTFMSG:" + des + "-" + mit)) {
                    System.out.println(message.getContent());
                    String msg=(String) message.getContent();
                    txtarea.appendText(msg);
                    message.setFlag(Flags.Flag.DELETED, true);
                }
            }

            inbox.close(true);
            store.close();
            Done.setVisible(true);

        }

        /*
        Icona di "DONE" per la GUI.
         */

     @FXML
    public void handleRefresh(){
            Done.setVisible(false);

    }
    }





