package sample;
/*
Questa classe gestisce le azioni sulla finestra di iscrizione.
 */
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.controlsfx.control.textfield.TextFields;

import javax.mail.MessagingException;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.net.URL;
import java.sql.*;
import java.util.Optional;
import java.util.ResourceBundle;
//gestisce tutti i metodi riguardanti la prima iscrizione del guest.

public class ControllerSignUp extends Main implements Initializable {
    private Main mainApp;
    private EmailUtility e;
    private String args;
    private static String url="jdbc:mysql://localhost:3306/meetfly?useSSL=false&serverTimezone=UTC";
    private static String username = "root";
    private static String password = "root";
    @FXML private TextField nameBox;
    @FXML private TextField surnameBox;
    @FXML private TextField emailAddressBox;
    @FXML private TextField usernameBox;
    @FXML private PasswordField pwdBox;
    @FXML private PasswordField repPwdBox;
    @FXML private Node wrongPass;
    @FXML private Node regSuc;
    @FXML private Node emptyFields;
    @FXML private Node RegEmail;
    @FXML private Node Regusername;
    @FXML private Node LenghtPwd;
    @FXML private Node Emailexist;
    @FXML
    private TextField Nationality;
    @FXML
    private TextField Sex;
    @FXML
    private TextField BirthYear;
    @FXML
    private TextField Phone;
    private String[] nations = new String[250];
    private String[] identity = new String[2];
    private String[] birth=new String[91];
   // @FXML private Text text;


    @Override
    //inizializza le textbox predittive
    public void initialize(URL location, ResourceBundle resources) {

        try {
            txtBoxNationality();
            txtBoxSex();
            txtBoxBirthYear();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        TextFields.bindAutoCompletion(Nationality, nations);
        TextFields.bindAutoCompletion(Sex, identity);
        TextFields.bindAutoCompletion(BirthYear, birth);
    }

    //estrae dal DB tutte le nazionalità disponibili
    public void txtBoxNationality() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT nationality FROM meetfly.countries");
        int i;
        i = 0;
        while (rs.next()) {
            nations[i] = rs.getString("nationality");
            i++;
        }
    }
    // Metodo che permette di inizializzare la predizione del sesso nella casella di testo
    public void txtBoxSex() {
        identity[0]="Male";
        identity[1]="Female";
    }

    // Metodo che permette di inizializzare la predizione degli anni  nella casella di testo
    public void txtBoxBirthYear(){
        int start_year=1920;
        for(int i=0;i<=90; i++){
            String y= Integer.toString(start_year);
            birth[i]= y;
            start_year++;
        }
        }

    //Metodo che gestisce il button del login effettuando il controllo sui campi
    //attivando eventuali etichette di avviso
    @FXML
    private void handleSignedIn() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        String name = nameBox.getText();
        String surname = surnameBox.getText();
        String emailAddress = emailAddressBox.getText();
        String username = usernameBox.getText();
        String pwd = pwdBox.getText();
        String repPwd = repPwdBox.getText();
        String nat = Nationality.getText();
        String sex = Sex.getText();
        String birthy = BirthYear.getText();
        String phone = Phone.getText();

        try {
            //Caso favorevole(Tutti i campi  sono riempiti e le password coincidono-> il sign up si conclude con successo)
            if (pwd.equals(repPwd) && !name.isEmpty() && !surname.isEmpty() && !emailAddress.isEmpty() && !username.isEmpty() && !pwd.isEmpty() && !repPwd.isEmpty() && !emailCheckDB(emailAddress) && !usernameCheckDB(username) && pwdCheck(pwd)) {
                wrongPass.setVisible(false);
                emptyFields.setVisible(false);
                RegEmail.setVisible(false);
                Regusername.setVisible(false);
                LenghtPwd.setVisible(false);
                Emailexist.setVisible(false);
                addUserOnDB(emailAddress, pwd, name, surname, username, nat, sex, birthy, phone);
                e.sendEmail("smtp.libero.it", "25", "meetfly2019@libero.it", "noreplymeetfly",
                        emailAddress, "Welcome to MeetFly " + name + "!", "Now you are a member of MeetFly Community :)\n\nMeetFly Staff\n");
                regSuc.setVisible(true);
            }// Caso di uno o più campi vuoti con attivazione di una label di errore.
            else if (name.isEmpty() || surname.isEmpty() || emailAddress.isEmpty() || username.isEmpty() || pwd.isEmpty() || repPwd.isEmpty()) {
                emptyFields.setVisible(true);
                RegEmail.setVisible(false);
                regSuc.setVisible(false);
                Regusername.setVisible(false);
                wrongPass.setVisible(false);
                LenghtPwd.setVisible(false);
                Emailexist.setVisible(false);
            }//Caso con controllo di email già inserita. Viene attivata una label di errore
            else if (emailCheckDB(emailAddress)) {
                RegEmail.setVisible(true);
                emptyFields.setVisible(false);
                regSuc.setVisible(false);
                Regusername.setVisible(false);
                wrongPass.setVisible(false);
                LenghtPwd.setVisible(false);
                Emailexist.setVisible(false);


            }//Caso in cui username è già esistente.Viene attivata una label di errore
            else if (usernameCheckDB(username)) {
                RegEmail.setVisible(false);
                emptyFields.setVisible(false);
                regSuc.setVisible(false);
                Regusername.setVisible(true);
                wrongPass.setVisible(false);
                LenghtPwd.setVisible(false);
                Emailexist.setVisible(false);

            } //se la password non rispetta la lunghezza imposta dal servizio
            else if (!pwdCheck(pwd)){
            emptyFields.setVisible(false);
            wrongPass.setVisible(false);
            regSuc.setVisible(false);
            RegEmail.setVisible(false);
            Regusername.setVisible(false);
            LenghtPwd.setVisible(true);
            Emailexist.setVisible(false);
            }
            //Caso in cui le password non coincidono, attivazione di una label a riguardo.
            else if (!pwd.equals(repPwd)) {
            emptyFields.setVisible(false);
            wrongPass.setVisible(true);
            regSuc.setVisible(false);
            RegEmail.setVisible(false);
            Regusername.setVisible(false);
            LenghtPwd.setVisible(false);
            Emailexist.setVisible(false);

        }
     }catch ( MessagingException m){
            m.printStackTrace();
            Emailexist.setVisible(true);
            emptyFields.setVisible(false);
            wrongPass.setVisible(false);
            regSuc.setVisible(false);
            RegEmail.setVisible(false);
            Regusername.setVisible(false);
            LenghtPwd.setVisible(false);
            deleteUserOnDB(emailAddress);

     }
    }

    //Metodo che inserisce i dati del nuovo user nel database.
    public void addUserOnDB(String usr,String pwd,String nm,String sn,String sur,String nat, String sex, String birthy, String ph)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("INSERT INTO users (name,surname,emailAddress,password,username,nationality,sex,birthyear,phone) VALUES (?,?,?,?,?,?,?,?,?)");
        pstmt.setString(1,nm);
        pstmt.setString(2,sn);
        pstmt.setString(3,usr);
        pstmt.setString(4,pwd);
        pstmt.setString(5,sur);
        pstmt.setString(6,nat);
        pstmt.setString(7,sex);
        pstmt.setString(8,birthy);
        pstmt.setString(9,ph);
        pstmt.executeUpdate();
        conn.close();

    }

    //Metodo che evita l'inserimento dell'utente se l'email non è valida
    public void deleteUserOnDB(String usr)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("DELETE FROM users WHERE emailAddress=?");
        pstmt.setString(1,usr);
        pstmt.executeUpdate();
        conn.close();

    }
    //se l'email dell'user è già esistente, il metodo restituisce true

    public boolean emailCheckDB(String usr)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM meetfly.users WHERE  emailAddress LIKE ?");
        pstmt.setString(1,usr);
        ResultSet rs= pstmt.executeQuery();
        boolean result = rs.next();
        conn.close();
        return result;

    }
    //Verifica la presenza di nickname uguali
    public boolean usernameCheckDB(String usr)throws SQLException,ClassCastException,ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("SELECT * FROM meetfly.users WHERE  username LIKE ?");
        pstmt.setString(1,usr);
        ResultSet rs= pstmt.executeQuery();
        boolean result = rs.next();
        conn.close();
        return result;

    }

    //controllo di check lunghezza password
    public boolean pwdCheck(String password){
         if(password.length()<=5){
             return (false);
         }else return (true);
    }

}



