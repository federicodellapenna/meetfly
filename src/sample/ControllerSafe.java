package sample;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.*;
//gestisce la finestra di salvataggio dati a fronte della disconnessione dell'utente
public class ControllerSafe extends Main{
    private static String url="jdbc:mysql://localhost:3306/meetfly?useSSL=false&serverTimezone=UTC";
    String email;
    public void setEmailAddr(String e) { email = e; }

    @FXML
    //gestisce la cancellazione dei dati inseriti per la ricerca prima di compiere il logout, con relativa chiusura di tutte le finestre precedenti.
    //l'effetto è che mi trovo nuovamente in schermata home come guest.
    public void handleNotSave () throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection conn = DriverManager.getConnection(url, "root", "root");
        Statement st=conn.createStatement();
        PreparedStatement pstmt=conn.prepareStatement("UPDATE meetfly.users SET street=null , number=null , city=null , cityCode=null , airportCode=null , flightCode=null WHERE emailAddress=?");
        pstmt.setString(1, email);
        pstmt.executeUpdate();
        conn.close();
        MainSafeScene.getWindow().hide();
        MainLogoutScene.getWindow().hide();
        MainPrimaryScene.getWindow().hide();

    }

    @FXML
    //mantiene le informazioni utili alla ricerca anche dopo la disconnessione. Come il metodo precedente,
    //l'effetto è che ottengo nuovamente la schermata home come guest.
    public void handleSave(){
        MainSafeScene.getWindow().hide();
        MainLogoutScene.getWindow().hide();
        MainPrimaryScene.getWindow().hide();
    }
}
